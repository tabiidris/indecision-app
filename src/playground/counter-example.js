class Counter extends React.Component{

    constructor(props) {
        super(props);
        this.addOne = this.addOne.bind(this);
        this.minusOne = this.minusOne.bind(this);
        this.reset = this.reset.bind(this);
        this.state = {
            count:0
        };
    }

    addOne(){
        this.setState((prevState)=>{
            return {
                count: prevState.count + 1
            };
        });
    }
    minusOne(){
        this.setState((prevState)=>{
            return {
                count: prevState.count - 1
            };
        });
    }
    reset(){
        this.setState((prevState)=>{
            return {
                count: 0
            };
        });
    }
    render(){
        return(
            <div>
                <hi>Count: {this.state.count}</hi>
                <p>
                    <button onClick={this.addOne}>+1</button>
                    <button onClick={this.minusOne}>-1</button>
                    <button onClick={this.reset}>reset</button>
                </p>   
            </div>
        );
    }
}

ReactDOM.render(<Counter />, document.getElementById('app'));

// const renderCounterApp = () => {
//     const templateTwo = (
//         <div>
//             <h1>Count: {count}</h1>
//             <button onClick={addOne} > +1 </button>
//             <button onClick={minusOne} > -1 </button>
//             <button onClick={reset} > reset </button>
//         </div>
//     );
// };

// const appRoot = document.getElementById('app');

// ReactDOM.render(template, appRoot);

// let count = 0;

// const addOne = () =>{
//     count ++; 
//     console.log('addOne');
// };

// const minusOne = () =>{
//     console.log('minusOne');
// };

// const reset = () =>{
//     console.log('reset');
// }

