import React from 'react';

// class Options extends React.Component {
//   render() {
//     return (
//       <div>
//         <button onClick={this.props.handleDeleteOptions}>Remove All</button>
//         {
//           this.props.options.map((option) => <Option key={option} optionText={option} />)
//         }
//       </div>
//     );
//   }
// }

const Option = (props) => (
    <div className="option">
        <p className="text">{props.count}. {props.optionText}</p>
        <button
            className="button button--link"
            onClick={()=> {props.handleDeleteOption(props.optionText);} }
        >
            Remove
        </button>
    </div>
);

// class Option extends React.Component {
//   render() {
//     return (
//       <div>
//         {this.props.optionText}
//       </div>
//     );
//   }
// }
 export default Option;

