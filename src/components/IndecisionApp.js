import React from "react";
import Header from "./Header";
import Action from "./Action";
import Options from "./Options";
import AddOption from "./AddOption";
import OptionModal from './OptionModal'; 


class IndecisionApp extends React.Component {
    state = {
        options: [],
        selectedOption: undefined
    };
    handleDeleteOptions = () => {
        this.setState(() => ({ options: []}));
    }
    handleDeleteOption = (optionToRemove) => {
        this.setState((prevState) => ({
            options:prevState.options.filter((option)=> optionToRemove !== option )
        }));
    }
    handlePick = () => {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const option = this.state.options[randomNum];
        this.setState(() => ({
            selectedOption: option
        }));
    }
    handleClearModal = () => {
        this.setState( () => ({selectedOption:undefined}));
    }
    handleAddOption = (option) => {
        if(!option){
            return 'Enter a valid text please!';
        }else if(this.state.options.indexOf(option) > -1 ){
            return 'This option already exist';
        }
        this.setState((prevState) => ({options: prevState.options.concat(option)}));
    }
    componentDidMount(){
        try {
            const mydata = localStorage.getItem('options');
            const options = JSON.parse(mydata);

            if(options){
                this.setState(() =>  ({ options }))
            }
        } catch (e) {
            //Do nothing at all
        }
    }
    componentDidUpdate(prevProps, prevState){
        if(prevState.options.length !== this.state.options.length){
            const mydata = JSON.stringify(this.state.options);
            localStorage.setItem('options', mydata);
        }
    }
    componentWillUnmount(){

    }

    render() {
        const subtitle = 'Put your life in the hands of a computer.';

        return (
            <div>
                <Header subtitle={subtitle} />
                <div className="container">
                    <Action
                        hasOptions={this.state.options.length > 0}
                        handlePick={this.handlePick}
                    />
                    <div className="widget">
                        <Options
                        options={this.state.options}
                        handleDeleteOptions={this.handleDeleteOptions}
                        handleDeleteOption = {this.handleDeleteOption}
                        />
                        <AddOption
                            handleAddOption = {this.handleAddOption}
                        />
                    </div>
                </div>
                    <OptionModal 
                    selectedOption = {this.state.selectedOption}
                    handleClearModal = {this.handleClearModal}
                    />
            </div>
        );
    }
}

IndecisionApp.defaultProps = {
    options: []
};


export default IndecisionApp;